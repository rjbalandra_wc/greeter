//
//  Greetable+Extension.swift
//  GreeterUITests
//
//  Created by RJ Balandra on 13/11/2018.
//  Copyright © 2018 RJ Balandra. All rights reserved.
//

import XCTest
extension Greetable {
    
    func givenTheAppLaunched() {
        XCUIApplication().launch()
    }
    
    func thenIShouldSeeGreetButton() {
        XCTAssertTrue(XCUIApplication().buttons["Greet"].exists)
    }
    
    func whenIPressGreetButton() {
        GreeterElements.greetButton.tap()
    }
    
    func thenIShouldSeeWeocomeMessage() {
          XCTAssertTrue(GreeterElements.welcomeText.exists)
    }
    
}
