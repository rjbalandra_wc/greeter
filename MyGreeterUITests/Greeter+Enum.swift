//
//  Greeter+Enum.swift
//  GreeterUITests
//
//  Created by RJ Balandra on 13/11/2018.
//  Copyright © 2018 RJ Balandra. All rights reserved.
//

import XCTest

enum GreeterElements {
    
    static let greetButton = XCUIApplication().buttons["Greet"]
    static let welcomeText = XCUIApplication().staticTexts["Welcome to POP"]
    
}
