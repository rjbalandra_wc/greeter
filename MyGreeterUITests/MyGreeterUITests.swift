//
//  MyGreeterUITests.swift
//  MyGreeterUITests
//
//  Created by RJ Balandra on 13/11/2018.
//  Copyright © 2018 RJ Balandra. All rights reserved.
//

import XCTest

class MyGreeterUITests: XCTestCase, Greetable {

    override func setUp() {
        super.setUp()
        continueAfterFailure = false
    }
    override func tearDown() {
        super.tearDown()
    }
    func testHomeScreenHasGreetButton() {
        
        givenTheAppLaunched()
        thenIShouldSeeGreetButton()
        
    }
    func testUserShouldGetWelcomeMessageOnceEntered() {
        
        givenTheAppLaunched()
        thenIShouldSeeGreetButton()
        whenIPressGreetButton()
        thenIShouldSeeWeocomeMessage()
        
        
    }

}
