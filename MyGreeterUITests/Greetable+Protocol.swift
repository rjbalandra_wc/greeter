//
//  Greetable+Protocol.swift
//  GreeterUITests
//
//  Created by RJ Balandra on 13/11/2018.
//  Copyright © 2018 RJ Balandra. All rights reserved.
//

protocol Greetable {
    func testHomeScreenHasGreetButton()
    func testUserShouldGetWelcomeMessageOnceEntered()
}
