//
//  ViewController.swift
//  Greeter
//
//  Created by RJ Balandra on 13/11/2018.
//  Copyright © 2018 RJ Balandra. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var welcomeLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func greetUser(_ sender: Any) {
        welcomeLabel.text = "Welcome to POP"
    }
    
}

